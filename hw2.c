#include "wordsearch.h"
#include <stdio.h>
#include <string.h>
#define MAXSZ 11
// NOTE: this file should not contain any functions other than main();
// other functions should go in wordsearch.c, with their prototypes in
// wordsearch.h

int main(int argc, char* argv[]) {
  if(argc == 1)
    printf("Usage: grid.txt\n");
  printf("Please enter the words to be searched for, seperated by a space.\n");
  FILE* infile = fopen(argv[1], "r");
  FILE* outfile = fopen("output.txt", "w");
  char grid[MAXSZ][MAXSZ];
  int n = load_grid(infile, grid);
  char w[MAXSZ];
  char args[100];
  fgets(args, 100, stdin);
  args[strlen(args)-1] = '\0';
  while(sscanf(args, "%s", w) != EOF){
    write_all_matches(outfile, grid, n, w);
    if(strlen(args) == strlen(w))
      break;
    for(unsigned int x = 0; x < strlen(args) - strlen(w) + 1; x++){
      args[x] = args[strlen(w) + x + 1];
    }
    args[strlen(args)] = '\0';
  }
  return 0;
}
