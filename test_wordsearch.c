#include "wordsearch.h"
#include <stdio.h>
#include <assert.h>
#include <stdbool.h>

/*
 * Reads lhs and rhs character by character until either reaches eof.
 * Returns true if the files can be read and the two files match
 * character by character. Returns false if two files either can't be
 * opened or don't match.
 */
bool fileeq(char lhsName[], char rhsName[]) {
    FILE* lhs = fopen(lhsName, "r");
    FILE* rhs = fopen(rhsName, "r");

    // don't compare if we can't open the files
    if (!lhs || !rhs) return false;

    bool match = true;
    // read until both of the files are done or there is a mismatch
    while (!feof(lhs) || !feof(rhs)) {
        if (feof(lhs) ||                  // lhs done first
            feof(rhs) ||                  // rhs done first
            (fgetc(lhs) != fgetc(rhs))) { // chars don't match
            match = false;
            break;
        }
    }
    fclose(lhs);
    fclose(rhs);
    return match;
}

/*
 * Test fileeq on same file, files with same contents, files with
 * different contents and a file that doesn't exist
 */
void test_fileeq() {
    FILE* fptr = fopen("test1.txt", "w");
    fprintf(fptr, "this\nis\na test\n");
    fclose(fptr);

    fptr = fopen("test2.txt", "w");
    fprintf(fptr, "this\nis\na different test\n");
    fclose(fptr);

    fptr = fopen("test3.txt", "w");
    fprintf(fptr, "this\nis\na test\n");
    fclose(fptr);

    assert(fileeq("test1.txt", "test1.txt"));
    assert(fileeq("test2.txt", "test2.txt"));
    assert(!fileeq("test2.txt", "test1.txt"));
    assert(!fileeq("test1.txt", "test2.txt"));
    assert(fileeq("test3.txt", "test3.txt"));
    assert(fileeq("test1.txt", "test3.txt"));
    assert(fileeq("test3.txt", "test1.txt"));
    assert(!fileeq("test2.txt", "test3.txt"));
    assert(!fileeq("test3.txt", "test2.txt"));
    assert(!fileeq("", ""));  // can't open file
}


/*
 * writes out a hard-coded grid to a file;
 * uses load_grid to read in the grid;
 * test that the loaded grid is correct
 */
void test_load_grid() {
  char test_grid[MAXSZ][MAXSZ];
  
  FILE* fptr = fopen("test1.txt", "w");
  fprintf(fptr, "a\n");
  fclose(fptr);

  FILE* frdr = fopen("test1.txt", "r");
  assert(load_grid(frdr, test_grid) == 1);
  fclose(frdr);

  fptr = fopen("test1.txt", "w");
  fprintf(fptr, "qwertyuiop\nasdfghjklz\nxcvbnmqwer\ntyuiopasdf\nghjklzxcvb\nnmqwertyui\nopasdfghjk\nlzxcvbnmqw\nertyuiopas\ndfghjklzxc\n");
  fclose(fptr);
  frdr = fopen("test1.txt", "r");
  assert(load_grid(frdr, test_grid) == 10);
  fclose(frdr);

  // nothing in the file
  fptr = fopen("test1.txt", "w");
  fclose(fptr);
  frdr = fopen("test1.txt", "r");
  assert(load_grid(frdr, test_grid) == 0);
  fclose(frdr);

  // rows have differing numbers of characters
  fptr = fopen("test1.txt", "w");
  fprintf(fptr, "one\ntwo\nthree\n");
  fclose(fptr);
  frdr = fopen("test1.txt", "r");
  assert(load_grid(frdr, test_grid) == 0);
  fclose(frdr);

  // number of rows doesn't match number of columns
  fptr = fopen("test1.txt", "w");
  fprintf(fptr, "a\nb\nc\n");
  fclose(fptr);
  frdr = fopen("test1.txt", "r");
  assert(load_grid(frdr, test_grid) == 0);
  fclose(frdr);
}

void test_find_forward() {
  char test_grid[MAXSZ][MAXSZ];
  FILE* fptr = fopen("test1.txt", "w");
  fprintf(fptr, "aaaa\naaaa\nfind\naaaa\n");
  fclose(fptr);
  FILE* frdr = fopen("test1.txt", "r");
  load_grid(fptr, test_grid);
  assert(find_word_right(test_grid, 4, "find", 2, 0));
  assert(!find_word_right(test_grid, 4, "find", 3, 3));
  fclose(frdr);
}
void test_find_backward() {
  char test_grid[MAXSZ][MAXSZ];
  FILE* fptr = fopen("test1.txt", "w");
  fprintf(fptr, "aaaa\naaaa\ndnif\naaaa\n");
  fclose(fptr);
  FILE* frdr = fopen("test1.txt", "r");
  load_grid(fptr, test_grid);
  assert(find_word_left(test_grid, 4, "find", 2, 3));
  assert(!find_word_left(test_grid, 4, "find", 3, 3));
  fclose(frdr);
}
void test_find_up() { 
  char test_grid[MAXSZ][MAXSZ];
  FILE* fptr = fopen("test1.txt", "w");
  fprintf(fptr, "adaa\nanaa\naiaa\nafaa\n");
  fclose(fptr);
  FILE* frdr = fopen("test1.txt", "r");
  load_grid(fptr, test_grid);
  assert(find_word_up(test_grid, 4, "find", 3, 1));
  assert(!find_word_up(test_grid, 4, "find", 3, 3));
  fclose(frdr);
}
void test_find_down() {
  char test_grid[MAXSZ][MAXSZ];
  FILE* fptr = fopen("test1.txt", "w");
  fprintf(fptr, "afaa\naiaa\nanaa\nadaa\n");
  fclose(fptr);
  FILE* frdr = fopen("test1.txt", "r");
  load_grid(fptr, test_grid);
  assert(find_word_down(test_grid, 4, "find", 0, 1));
  assert(!find_word_down(test_grid, 4, "find", 3, 3));
  fclose(frdr);
}

void test_write_all_matches() {
  char test_grid[MAXSZ][MAXSZ];
  FILE* fptr = fopen("test1.txt", "w");
  fprintf(fptr, "OOOdOOO\nOOOnOOO\nOOOiOOO\ndnifind\nOOOiOOO\nOOOnOOO\nOOOdOOO\n");
  fclose(fptr);
  fptr = fopen("test2.txt", "w");
  fprintf(fptr, "find 3 3 U\nfind 3 3 D\nfind 3 3 L\nfind 3 3 R\n");
  fclose(fptr);
  FILE* frdr = fopen("test1.txt", "r");
  load_grid(frdr, test_grid);
  fclose(frdr);
  fptr = fopen("test1.txt", "w");
  write_all_matches(fptr, test_grid, 7, "find");
  fclose(fptr);
  assert(fileeq("test1.txt", "test2.txt"));

  fptr = fopen("test2.txt", "w");
  fprintf(fptr, "nope - Not Found\n");
  fclose(fptr);
  fptr = fopen("test1.txt", "w");
  write_all_matches(fptr, test_grid, 7, "nope");
  fclose(fptr);
  assert(fileeq("test1.txt", "test2.txt"));
}


int main(void) {
    printf("Testing fileeq...\n");
    test_fileeq();
    printf("Passed fileeq tests.\n");

    printf("Running wordsearch tests...\n");
    test_load_grid();
    test_find_forward();
    test_find_backward();
    test_find_up();
    test_find_down();
    test_write_all_matches();
    printf("Passed wordsearch tests.\n");
}
