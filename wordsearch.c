#include "wordsearch.h"
#include <ctype.h>
#include <stdio.h>
#include <assert.h>

// NOTE: all the functions declared in wordsearch.h should be
// implemented in this file.
int load_grid(FILE* infile, char outgrid[][MAXSZ]){
  if(infile == NULL){
    printf("Cannot open input file\n");
    return 0;
  }
  char c;
  int row = 0;
  int col = 0;
  int lastcol = 0;
  while((c = fgetc(infile)) != EOF){
    if(c == '\n'){
      outgrid[row][col] = '\0';
      row++;
      if(row != 1 && lastcol != col){
	printf("(1)Please enter an NxN grid where N is at most 10.\n");
	return 0;
      }
      lastcol = col;
      col = 0;
      if(row > MAXSZ){
	printf("(2)Please enter an NxN grid where N is at most 10.\n");
	return 0;
      }
      if((c = fgetc(infile)) == EOF)
	break;
    }
    if(col > MAXSZ){
      printf("(3)Please enter an NxN grid where N is at most 10.\n");
      return 0;
    }
    if(isalpha(c)){
      outgrid[row][col] = tolower(c);
    }
    col++;
  }
  if(row != lastcol){
    printf("(4)Please enter an NxN grid where N is at most 10.\n");
    return 0;
  }
    if(row == 0 || lastcol == 0){
      printf("(5)Please enter an NxN grid where N is at most 10.\n");
  return 0;
  }
  return row;
}

// Return true if the word 'w' is in the n x n grid starting at the
// given i, j position and moving in the specified direction; n
// specifies the length and width of the grid (try to avoid code
// duplication in these; you may want other helper functions that they
// can all use)
bool find_word_up(char grid[][MAXSZ], int n, char w[], int i, int j){
  if(i >= n || j >= n)
    return false;
  int a =0;
  while(w[a] != '\0'){
    if(i-a >= 0 && grid[i-a][j] == w[a])
      a++;
    else return false;
  }
  return true;
}
bool find_word_down(char grid[][MAXSZ], int n, char w[], int i, int j){
  int a = 0;
  while(w[a] != '\0'){
    if(i+a < n && grid[i+a][j] == w[a])
      a++;
    else return false;
  }
  return true;
}
bool find_word_left(char grid[][MAXSZ], int n, char w[], int i, int j){
  int a =0;
  if(i >= n || j >= n)
    return false;
  while(w[a] != '\0'){
    if(j-a >= 0 && grid[i][j-a] == w[a])
      a++;
    else return false;
  }
  return true;
}
bool find_word_right(char grid[][MAXSZ], int n, char w[], int i, int j){
  int a =0;
  while(w[a] != '\0')
    if(j+a < n && grid[i][j+a] == w[a]){
      a++;
    }
    else return false;
  return true;
}
   
// writes the row, column, and direction of every instance of the
// character given ; length of the array is determined by the size of
// the nxn grid.  searches the n x n grid for the given word, w, and
// prints the matches to outfile; n specifies the length and width of
// the grid (which is square)
void write_all_matches(FILE* outfile, char grid[][MAXSZ], int n, char w[]){
  bool found = false;
  for(int row = 0; row < n; row++){
    for(int col = 0; col < n; col++){
      if(find_word_up(grid, n, w, row, col)){
	fprintf(outfile, "%s %d %d U\n", w, row, col);
	found = true;
      }
      if(find_word_down(grid, n, w, row, col)){
	fprintf(outfile, "%s %d %d D\n", w, row, col);
	found = true;
      }
      if(find_word_left(grid, n, w, row, col)){
	fprintf(outfile, "%s %d %d L\n", w, row, col);
	found = true;
      }
      if(find_word_right(grid, n, w, row, col)){
	fprintf(outfile, "%s %d %d R\n", w, row, col);
	found = true;
      }
    }
  }
  if(!found)
    fprintf(outfile, "%s - Not Found\n", w);
}
